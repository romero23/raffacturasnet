﻿using NETProjectTutorial.dao;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NETProjectTutorial.entities;

namespace NETProjectTutorial.implements
{
    class DaoImplementEmpleado : IDaoEmpleado
    {
        //header cliente
        private BinaryReader brhempleado;
        private BinaryWriter bwhempleado;
        //data cliente
        private BinaryReader brdempleado;
        private BinaryWriter bwdempleado;

        private FileStream fshempleado;
        private FileStream fsdempleado;

        private const string FILENAME_HEADER = "hempleado.dat";
        private const string FILENAME_DATA = "dempleado.dat";
        private const int SIZE = 390;


        public DaoImplementEmpleado() { }

        private void open()
        {
            try
            {
                fsdempleado = new FileStream(FILENAME_DATA,
                    FileMode.OpenOrCreate, FileAccess.ReadWrite);
                if (!File.Exists(FILENAME_HEADER))
                {
                    fshempleado = new FileStream(FILENAME_HEADER,
                        FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    brhempleado = new BinaryReader(fshempleado);
                    bwhempleado = new BinaryWriter(fshempleado);

                    brdempleado = new BinaryReader(fsdempleado);
                    bwdempleado = new BinaryWriter(fsdempleado);

                    bwhempleado.BaseStream.Seek(0, SeekOrigin.Begin);
                    bwhempleado.Write(0);//n
                    bwhempleado.Write(0);//k
                }
                else
                {
                    fshempleado = new FileStream(FILENAME_HEADER,
                        FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    brhempleado = new BinaryReader(fshempleado);
                    bwhempleado = new BinaryWriter(fshempleado);
                    brdempleado = new BinaryReader(fsdempleado);
                    bwdempleado = new BinaryWriter(fsdempleado);
                }
            }
            catch (IOException e)
            {
                throw new IOException(e.Message);
            }
        }

        private void close()
        {
            try
            {
                if (brdempleado != null)
                {
                    brdempleado.Close();
                }
                if (brhempleado != null)
                {
                    brhempleado.Close();
                }
                if (bwdempleado != null)
                {
                    bwdempleado.Close();
                }
                if (bwhempleado != null)
                {
                    bwhempleado.Close();
                }
                if (fsdempleado != null)
                {
                    fsdempleado.Close();
                }
                if (fshempleado != null)
                {
                    fshempleado.Close();
                }
            }
            catch (IOException e)
            {
                throw new IOException(e.Message);
            }
        }

        public Empleado findById(int id)
        {
            throw new NotImplementedException();
        }

        public Empleado findByCedula(string cedula)
        {
            throw new NotImplementedException();
        }

        public List<Empleado> findByLastname(string lastname)
        {
            throw new NotImplementedException();
        }

        public void save(Empleado t)
        {
            throw new NotImplementedException();
        }

        public int update(Empleado t)
        {
            throw new NotImplementedException();
        }

        public bool delete(Empleado t)
        {
            throw new NotImplementedException();
        }

        public List<Empleado> findAll()
        {
            open();
            List<Empleado> clientes = new List<Empleado>();

            brhempleado.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brhempleado.ReadInt32();
            for (int i = 0; i < n; i++)
            {
                //calculamos posicion cabecera
                long hpos = 8 + i * 4;
                brhempleado.BaseStream.Seek(hpos, SeekOrigin.Begin);
                int index = brhempleado.ReadInt32();
                //calculamos posicion de los datos
                long dpos = (index - 1) * SIZE;
                brdempleado.BaseStream.Seek(dpos, SeekOrigin.Begin);

                int id = brdempleado.ReadInt32();
                string nombres = brdempleado.ReadString();
                string apellidos = brdempleado.ReadString();
                string cedula = brdempleado.ReadString();
                string inss = brdempleado.ReadString();
                string direccion = brdempleado.ReadString();
                double salario = brdempleado.ReadDouble();


                string telefono = brdcliente.ReadString();
                string correo = brdcliente.ReadString();
                
                Cliente c = new Cliente(id, cedula, nombres,
                    apellidos, telefono, correo, direccion);
                clientes.Add(c);
            }

            close();
            return clientes;
        }
    }
}
